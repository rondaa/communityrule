---
layout: guide
title: "Create your first rule"
permalink: /guides/first-rule/
---

Whether your community is already up and running or still just an ambition, choosing and adopting an initial Rule can be a challenge. Some community members might resist the idea, preferring a more informal way of operating. But for almost any kind of community, there are benefits to having governance plans in place---even if most of the time they're just sitting in the background. Like a sewage system, a governance system may be working best when it goes unnoticed.

**Describe the reality, not the ideal.** Start with where you are. If your community values democracy but currently runs as an autocracy, own up to it in your Rule. You might also include in your Rule the intention of changing in the future. But sometimes the best way to spur change is to hold a mirror up to how the community is presently operating. Start by describing briefly and clearly how your community operates now.

**It may be easiest to start with a [template]({% link templates.md %}), but you can also [start from scratch]({% link create.md %}).** On a template, press the <span class="pushButton">Fork</span> button at the bottom to get started. Even if you end up changing almost everything you find in a template, the process can spur thinking in ways that a blank canvas would not.

**Drag and drop modules to design your Rule.** Under the community name and basic structure, you can start dragging pre-defined or custom modules into your Rule. You can drag a module into another module to show how the parent module works in greater detail.

**If you use the modules, be sure to explain what they mean.** When you add a module or click on its name, a field appears that allows you to describe the module in your own words. Text typed there automatically becomes part of the module, and it will appear when you hover the cursor over its name. For inspiration, you can press the ![More info]({% link assets/tabler_icons/info-circle.svg %}) icon on pre-defined modules to learn more about them.

**Separate the Rule from its data**. Think of your Rule like a constitution as opposed to a code of laws, like a building as opposed to the people who put it to use. Don't try to include all your community's policies in your Rule; explain there how you make and change policies. Don't list out who holds what roles; just define the roles. In your Rule, make clear where people can find that data---the policies, the role-holders, and so forth.

**Read and re-read.** Be sure that all the terminology and processes are consistent. Inevitably there will be loopholes and bugs, but try to resolve as many of them as you can. The <span class="pushButton">Preview</span> button provides a clean display for easy reading. Just press <span class="pushButton">Customize</span> when you're ready to edit again. Show it to your community for feedback.

**When you're ready to adopt your Rule, put it in a place where your community can easily find it.** Depending on the spaces and tools your community uses, this can mean different things. But one way or another, make your Rule accessible. CommunityRule offers two options: [Publish your Rule to the Library]({% link _guides/publish_rule.md %}) (and provide your community with a link), or export it as a Markdown file (which you can add to your own website or [Git repository]({% link _guides/git_repo.md %}).

**Prepare for your Rule to evolve.** Make sure you and your Rule are prepared for change. Often, soon after a Rule is up and running, simple problems arise that would have been hard to notice or predict on paper. Celebrate once you've adopted your first Rule, but let it be a living document.
