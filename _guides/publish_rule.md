---
layout: guide
title: "Publish a Rule on CommunityRule"
permalink: /guides/publish-rule/
---

**CommunityRule is not just a tool for creating governance documents, it's a platform for publishing and sharing them.** Rules that have already been published are available at the [Library]({% link library.md %}). You can publish both works in progress and actively adopted Rules. You can also keep earlier drafts of a Rule in the Library; each published Rule has a unique URL and timestamp.

Note that, currently, the Library tool is quite crude and published Rules cannot be edited once published.

**Once you've written, edited, and re-edited your Rule, press <span class="pushButton">Publish</span>.** This will generate a unique URL for your Rule and display it. You can now link to this URL and use it as the official version for your community. The Rule is now also publicly available in the Library.

**Your Rule is automatically licensed for re-use.** As a condition of using CommunityRule, published Rules are made available under a Creative Commons BY-SA license. That means others are free to adapt your Rule and should credit the source.

**You or others can fork your Rule to build on it.** "Forking" means taking an existing product, copying it, and revising it into something new. You can use this feature to create a revision of your Rule, and others can use it to generate their own Rule based on yours.

**You can also delete your Rule.** Currently, if you press the <span class="pushButton"><img src="{% link assets/tabler_icons/trash.svg %}" /></span> button, it should open an email to CommunityRule's developers for a deletion request. We will work on streamlining this process so that it will be more straightforward for Rule creators to delete their own Rule.
