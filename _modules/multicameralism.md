---
layout: module
title: Multicameralism
permalink: /modules/multicameralism/
summary: Within a particular role in governance, there are two distinct entities that must independently agree on a decision for it to pass.
type: structure
---

Multicameralism refers to the organization of a legislature into separate, distinct bodies. It is an enactment of the separation of powers; it divides decision-making among different chambers or houses. The assembly can be divided into as many assemblies as necessary – some have two branches while others may have four or even five.


**Input:** distinct legislative bodies with sovereignty over different aspects of governmental decision-making

**Output:** a system of chambers that divides power among two or more groups

## Background

Some of the oldest examples of multicameral legislatures can be found in various European parliaments. The oldest surviving parliament is the British Parliament, established in Anglo-Saxon times, which had law-making and law-enforcement councils that eventually evolved into the bicameral legislature the Parliament implements today.

A tricameral legislature is traditionally associated with Simon Bolivar’s theory in “which a popularly elected chamber, (the Chamber of Tribunes) would be endowed with the power to regulate finance and foreign affairs, a hereditary chamber (the Senate) would enact law, and a third chamber (the Censors) would have the power to review the lawfulness of the acts of the other two and to protect fundamental rights.” ([Passaglia, 2018](https://www.degruyter.com/downloadpdf/j/pof.2018.10.issue-2/pof-2018-0014/pof-2018-0014.pdf)). This theory, though, remains as such – parliamentary governments were never popularized in the Americas.

Apartheid-era South African government instituted a tricameral system with race-based houses, abolished in 1994. A tetracameral legislature was implemented in Finland until 1906. Most European parliaments presently employ a unicameral or bicameral system.

## Feedback loops

### Sensitivities

* Provides checks and balances for legislation, preventing abuse of power and dictatorship
* Can provide representation for individuals on a more accurate level
* Can result in laws that are vetted, better-developed, and overall more beneficial to the public

### Oversights

* May not actually represent population it is supposed to serve
* Can result in deadlock, especially in a bicameral system, and thus waste resources
* Can be manipulated through means like gerrymandering
* May dramatically slow lawmaking process

## Implementations

### Communities

The UK’s Parliament and the United States Congress are both bicameral democracies – the US Congress has the Senate and House of Representatives while Parliament has the House of Commons and the House of Lords. 

Other organizational bodies can implement a multicameral legislature as well, such as that [proposed for Canadian healthcare advancement by Carson and Nossal (2016)](https://ebookcentral.proquest.com/lib/ucb/reader.action?docID=4851673); collaboration between an operating board of directors and a policy council would form a bicameral governance structure. Some university governance, like that of [Dalhousie University](https://cdn.dal.ca/content/dam/dalhousie/pdf/dept/university_secretariat/Board-of-Governors/Governance%20Structure%20Document%20-%20updated%20August%202016.pdf), is bicameral, as University Administration and Board of Governors are divided in their legislative responsibilities; the [University of Alberta](https://cloudfront.ualberta.ca/-/media/universitygovernance/documents/what-we-do/governance/gov101september-112018.pdf) employs a similar structure.

### Tools

[Principles of Successful Bicameral Governance](https://www.kpu.ca/sites/default/files/downloads/guiding_principles_bicameral_201223724.pdf) from Kwantlen Polytechnic University

## Further resources

* Brauninger, T. (2003). When simple voting doesn't work: Multicameral systems for the representation and aggregation of interests in international organizations. British Journal of Political Science, 33, 681-704. doi:http://dx.doi.org.colorado.idm.oclc.org/10.1017/S0007123403000310
* Passaglia, P. (2018). Unicameralism, Bicameralism, Multicameralism: Evolution and Trends in Europe. Perspectives on Federalism, 10(2), 1-29.
* Trakman, L. (2008). Modelling university governance. Higher Education Quarterly, 62(1‐2), 63-83.
* Tsebelis, G. (1995). Decision making in political systems: Veto players in presidentialism, parliamentarism, multicameralism and multipartyism. British journal of political science, 25(3), 289-325.
