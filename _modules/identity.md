---
layout: module
title: Identity
permalink: /modules/identity/
summary: A mechanism for identifying participants as appropriate for the community.
type: process
---
