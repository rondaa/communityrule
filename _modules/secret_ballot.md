---
layout: module
title: Secret Ballot
permalink: /modules/secret_ballot/
summary: A vote, once cast, cannot be traced to the voter's identity.
type: process
---

<!--
Article 21.3 of the [Universal Declaration of Human Rights](https://www.un.org/en/universal-declaration-human-rights/index.html) regards secret ballots as a basic right by stating:

> The will of the people shall be the basis of the authority of government; this will shall be expressed in periodic and genuine elections which shall be by universal and equal suffrage and shall be held by secret vote or by equivalent free voting procedures.
-->
