---
layout: module
title: Code of Conduct
permalink: /modules/Code_of_Conduct/
summary: Participants agree to abide by certain standards or face certain consequences.
type: process
---


**Input:** 

**Output:** 

## Background


## Feedback loops

### Sensitivities

* 

### Oversights

* 

## Implementations

### Communities

### Tools

* [Compassion Contract](https://citizendiscourse.org/compassion-contract/)
* [Contributor Covenant](http://contributor-covenant.org)

## Further resources

* 

