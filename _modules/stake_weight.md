---
layout: module
title: Stake Weight
permalink: /modules/stake_weight/
summary: Participants hold power in proportion to their investment in the organization.
type: decision
---
