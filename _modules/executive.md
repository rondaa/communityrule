---
layout: module
title: Executive
permalink: /modules/executive/
summary: A specific entity implements policy for the community.
type: structure
---
