---
layout: module
title: Consensus
permalink: /modules/constitution/
summary: A shared set of agreements underlies all future ones and is harder to change than other agreements.
type: structure
---

A constitution is a method for outlining the limits and regulations of an organization, nation, or other group’s governance. It may be composed as a single, succinct document or comprised of a set of acts, treaties, and particular court cases. 

**Input:** mission, purpose, and goals of an organization; an understanding of how the organization will function; long-term conception of the organization

**Output:** document(s) defining the scope of the group, its structures, functions, and eventual goals

## Background

Constitutions have a long history, with the oldest evidence dating back to Hammurabi's code of justice issued in modern-day Iraq circa 2300 BC. Well-known ancient codes of this nature include the Hittite code, and the oral codes of Athens.

Detailed modern, Western constitutions began with Oliver Cromwell’s Instrument of Government in England in the early 1650s. Most of the original thirteen American colonies adopted their own constitutions, with Connecticut’s being the oldest known North American constitution.

## Feedback loops

### Sensitivities

* Constitutions provide rules and checkpoints for an organization
* Keep power structures under control through division of powers into various branches
* Many include division of powers into various branches 
* Some groups, though not all, have a legal body to interpret the constitution and declare when acts violate it

### Oversights

* Many constitutions are difficult to change, preventing members of the organization to update and modernize them 
* Many include a “state of emergency” provision that can be used to violate the normally implemented rules should the president decide to do so, allowing an abuse of power 

## Implementations

### Communities

* Nations and smaller local organizations use constitutions as a means of governance
* School clubs, international groups, and nonprofit organizations often use constitutions to outline their purpose and keep mechanisms of power in check, such as:
    * [Kiwanis International](https://www.kiwanis.org/docs/default-source/training/governance/kiwanis-international/kiwanis-international-bylaws.pdf?sfvrsn=c1b22cef_20)
    * [National Medical Association](https://cdn.ymaws.com/www.nmanet.org/resource/resmgr/Docs/HOD/nma_constitution.pdf)
    * [American Civil Liberties Union](https://www.aclu.org/files/pdfs/about/aclu_bylaws.pdf)

### Tools

* University of Southern Indiana, "[How to Write a Constitution and Bylaws](https://www.usi.edu/media/959699/how_to_create_your_constitution_and_by-laws.pdf)"

## Further resources

Kerwin, C. M., & Furlong, S. R. (2018). _Rulemaking: How government agencies write law and make policy_. Cq Press.

Anckar, D. (2015). Prohibiting Amendment: the Use of Absolute Rigidity in the Constitutions of the Countries of the World. _Perspectivas-Journal of Political Science_, (14).
