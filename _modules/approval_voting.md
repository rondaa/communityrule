---
layout: module
title: Approval voting
permalink: /modules/approval_voting/
summary: Voters can approve or deny approval for each option.
type: decision
---

Approval voting is a form of plurality-majority voting in which each voter either positively or negatively determines all candidates’ aptitude for the job. Rather than only voting for one candidate who would best serve in the role, voters participating in approval voting either give or deny approval for each candidate and may approve as many as they see fit. The candidate with the most approval votes wins.

**Input:** ballot allowing for multiple approval or disapproval votes

**Output:** single- or multiple-winner election based on plurality approval

## Background

Approval voting as we now know it dates back to the work of Steven Brams and other political analysts in the 1970s. Its history can be traced to papal conclaves and Venetian Doge elections in the 13th through 18th centuries. Brams also recognizes its use during elections in 19th century England. 

## Feedback loops

### Sensitivities

* Approval voting has merit in its ability to capture the greatest support for a candidate. In elections with three or more candidates, it ensures that one with widespread support will win as opposed to one with default plurality. 
* Voters may express support for a minority candidate without “wasting” their vote; they may support both a minority and majority candidate. 
* Approval voting has also been credited with encouraging individuals to vote because a more diverse pool of applicants can be on the ballot. 
* Negative campaigning may also be less prominent in these systems.

### Oversights

* Voters may engage in "bullet voting," or only demonstrating approval for their top candidate. When this happens in a widespread fashion, the system returns to a standard plurality voting system.
* Candidates may water down their stances on particular controversial issues to appeal to a wide swath of voters.
* Can reproduce the issues associated with plurality-majority voting such as unfair representation, wasted votes, and inaccurate party representation.

## Implementations

### Communities

* Internal and local elections
* Private society elections such as Mathematical Association of America and the Institute of Electrical and Electronics Engineers 

### Tools

Structured ballot to facilitate voting by marks, names, written words “yes” or “no,” or selection

## Further resources

* Amy, D. J. (2000). Behind the ballot box: a citizen's guide to voting systems. Greenwood Publishing Group.
* Brams, S., & Fishburn, P. C. (2007). Approval voting. Springer Science & Business Media.


