---
layout: module
title: Access
permalink: /modules/access/
summary: A system for determining who has rights to control, change, or manage shared resources.
type: process
---
