---
layout: module
title: Legislature
permalink: /modules/legislature/
summary: A specific entity sets policy for the community.
type: structure
---
