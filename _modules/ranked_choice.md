---
layout: module
title: Ranked Choice
permalink: /modules/ranked_choice/
summary: Voters rank options, and the option with a majority of top-ranked votes wins; if there is no majority, options with the least top-ranked votes are removed until a majority appears.
type: decision
---


<!-- or, "instant runoff" -->

<!-- see FairVote -->
