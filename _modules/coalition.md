---
layout: module
title: Coalition
permalink: /modules/coalition/
summary: A network of diverse participants working toward a common aim.
type: structure
---

A coalition refers to the unification of groups, organizations, or individuals to work toward a shared goal as a team. Coalitions generally fall into two camps: internal or external. Internal coalitions are comprised of individuals who are already part of an organization, such as a workplace, while external coalitions are comprised of members of diverse organizations who join together to combine efforts. Coalitions exist at every level of governance, from local and community-based to international.

**Input:** individuals who have a shared mission, connect with each other, and want to enact something; a trigger that inspires formation (i.e. an event, a threat, or a piece of controversial legislation)

**Output:** a partnership of organizations or individuals working together to achieve a goal

## Background

Coalitions are traditionally associated with defeating a common enemy. Coalition warfare dates back to ancient Greece, when a coalition of city-states came together to ward off the Persian Empire.

## Feedback loops

### Sensitivities

* Increases audience and reach of a project
* Legitimizes efforts and boosts accountability
* Creates a larger pool of resources – both monetarily and in terms of human experience, knowledge, and specialization
* Increases productivity and capacity for putting pressure on institutions

### Oversights

* Power may be unequally distributed among organizations within the coalition
* Consensus and compromise can be time-consuming and difficult to achieve
* May hinder direct work for cause due to bureaucratic processes
* Members may have to compromise their position and its intensity for the greater purpose of the coalition

## Implementations

### Communities

* Coalitions frequently form in multiparty political systems like parliamentary governments
* Civic causes often have a coalition such as the Coalition for the Homeless

### Tools

Online toolkits for building a coalition can be found easily, outlining structures, systems of governance, and steps for building; the National Democratic Institute partnered with the Oslo Center for Peace and Human Rights to create a manual entitled *Coalitions: A Guide for Political Parties* with extensive information on best practices.

## Further resources

* Strength in Numbers: A Guide to Building Community Coalitions. (2003). Community Catalyst Report.
* Childress, B. (2019). Coalition Building. Reference for Business.


