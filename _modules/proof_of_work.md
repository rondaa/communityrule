---
layout: module
title: Proof of Work
permalink: /modules/proof_of_work/
summary: Power in decision-making is proportionate to a participant's labor for the community.
type: decision
---
