---
layout: module
title: Ritual
permalink: /modules/ritual/
summary: A repeated cultural practice builds solidarity and community.
type: culture
---

Ritual is a repeated practice that a given culture deems significant, either implicitly or explicitly. It typically has the effect of binding the community that practices it together and of reinforcing certain governance habits.

**Input:** community, culture, artful norm formation

**Output:** self-reinforcing common habits

## History

Ritual is among the "[human universals](https://en.wikipedia.org/wiki/Human_Universals)" that anthropologist Donald Brown argues can be found in every human society. It is a phenomenon frequently associated with religion, but it is by no means religion's exclusive purview. Rituals appear widely in childrearing, sports, the arts, and political life.

## Feedback loops

### Sensitivities

* Establishes and protects norms at lower cost of effort than other enforcement mechanisms, such as [judicial](judiciary.md) sanctions
* Connects and embeds governance processes organically into broader cultural systems

### Oversights

* Imposes expectations and habits that can be hard to change when change is necessary
* Can defy rational explanation and thus reduce the rationality of the governance system as a whole

## Implementations

### Communities

* [Beating the bounds](https://books.google.com/books?id=2kx7KiTEZCsC&lpg=PA74&pg=PA74#v=onepage&q&f=false), an ancient practice of collectively surveying community boundaries
* Courtroom practices of respect and costume for judges
* Inauguration ceremonies for public officials

### Tools

<!-- what software, methodologies, or organizations are available to facilitate implementation -->

## Further resources

* "[Rituals]," Wikimedia Commons
* Smith, Jonathan Z. _To Take Place: Toward Theory in Ritual_. University of Chicago Press, 1987.
