---
layout: module
title: Norms
permalink: /modules/norms/
summary: Informally agreed-on practices or beliefs.
type: culture
---
