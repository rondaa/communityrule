---
layout: module
title: Refusal
permalink: /modules/refusal/
summary: Participants withdraw participation because of discontent.
type: process
---
