---
layout: module
title: Restorative Justice
permalink: /modules/restorative_justice/
summary: Redress for wrong-doing occurs not through punishment but through repairing the harm done.
type: process
---
