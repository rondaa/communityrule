---
layout: module
title: Purpose
permalink: /modules/purpose/
summary: Shared goals for which the community exists.
type: culture
---
