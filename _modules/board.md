---
layout: module
title: Board
permalink: /modules/board/
summary: A small group of people shares top-level authority over an organization.
type: structure
---

Board governance is the use of a relatively small group of people, appointed or elected, to share authority and responsibility over a particular domain of decisions. Boards are widely used to set policy for corporations, nonprofit organizations, and governmental entities. They are also referred to as "councils," "trustees," or "directors."

Although boards take a wide variety of forms, they typically hold a legislative charge and delegate implementation of policies to a chief executive, whom the board has power to hire and fire. Some boards seek to "speak with one voice" while others make internal differences publicly apparent. Some boards are composed of full-time professionals, while many others involve part-time participation by outsiders to the organization's daily workings.

**Input:** rules for board-member selection, scope of authority, board procedures

**Output:** governing policies, executive delegation

## Background

The centrality of boards has been a presumption for the earliest modern corporate law.

## Feedback loops

### Sensitivities

* Focuses accountability for organizational behavior
* Can incorporate greater diversity of opinion than a single leader
* Holds monitoring power over executive functions
* May be accountable to constituents through election or political appointment

### Oversights

* Vulnerable to groupthink
* Outsider board members may have limited institutional knowledge
* May be unaccountable to constituents due self-appointment and lack of [term limit](term_limit.md)

## Implementations

### Communities

* Widespread use in various corporate bodies around the world

### Tools

* _Robert's Rules of Order_ is a classic manual for managing board meetings

## Further resources

* Carver, John and Miriam Mayhew Carver. _The Policy Governance Model and the Role of the Board Member_. Second edition. Jossey-Bass, 2009.
* Gevurtz, Franklin A. "[The Historical and Political Origins of the Corporate Board of Directors](https://scholarlycommons.law.hofstra.edu/cgi/viewcontent.cgi?article=2341&context=hlr)." _Hofstra Law Review_ 33, no. 1 (2004).
