---
layout: page
title: Guides
permalink: /guides/
---

These guides walk you through ways of using CommunityRule. Want a guide on another topic? Let us know.

**[Create your first rule]({% link _guides/first_rule.md %})**

**[Publish a Rule on CommunityRule]({% link _guides/publish_rule.md %})**

**[Add a Rule to a software project's Git repository]({% link _guides/git_repo.md %})**

**[Rules for Mutual Aid Groups]({% link _guides/mutual_aid.md %})**


