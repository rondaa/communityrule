---
layout: rule
title: Benevolent dictator
permalink: /templates/benevolent-dictator/
icon: /assets/tabler_icons/eye.svg

community-name: 

# BASICS
structure: One community member, the benevolent dictator (BD), holds ultimate decision-making power, aided by a Board. The Board will eventually become the basis of a more inclusive governance structure.
mission:
values: 
legal: 

# PARTICIPANTS
membership: Participation is open to anyone who wants to join.
removal: The BD can remove misbehaving participants at will for the sake of the common good.
roles: The BD invites active, committed participants to join the Board, whose members help the BD in managing the community.
limits: In the event that the BD is unable or unwilling to continue leadership, the BD may appoint a new BD or choose to alter the governance structure entirely.

# POLICY
rights: 
decision: The BD sets the community's policies and makes decisions for the community, taking reasonable account of input from other participants.
implementation: The BD is responsible for implementing—or delegating implementation of—policies and other decisions.
oversight: If participants are not happy with the BD's leadership, they may voice their concerns or leave the community.

# PROCESS
access:
economics:
deliberation: Community participants are free to discuss and debate community policies, practices, and culture.
grievances:

# EVOLUTION
records: 
modification: When the Board reaches 5 members, including the BD, the Board assumes control of the community. This control is activated upon the Board unanimously adopting a new Rule. Until then, the BD can change the governance structure of the community at will.

---

Use this template as-is or edit it to adapt it to your community.

A commonly used structure in open-source communities such as Linux, but replaces the idea of a "benevolent dictator for life" with a temporary benevolent dictator, as suggested by [a user-submitted Rule](https://communityrule.info/create/?r=1589491067257).
