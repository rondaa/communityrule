---
layout: rule
title: Jury
permalink: /templates/jury/
icon: /assets/tabler_icons/friends.svg

community-name: 

# BASICS
structure: Agreements are implemented through ad hoc juries made up of randomly selected members. A community administrator oversees the process and has the capacity to remove community members.
mission: 
values: Any community member has the right and responsibility to evaluate others' behavior.

# PARTICIPANTS
membership: 
removal: A member found by a jury to have broken an agreement once receives a warning. The second time, the member is suspended for one week. The third time, the member is suspended for one year, after which the process resets at the second stage.
roles:
limits: Jury members serve only one complaint at a time.

# POLICY
rights: This community regards the Contributor Covenant (contributor-covenant.org) as its body of policies.
decision: When a complaint about a community member is submitted to the administrator, a jury is formed to determine whether a violation has occurred. Juries consist of 5 community members selected at random for each complaint. If at least 4 members concur that the complaint accurately reflects the community agreements, the complaint triggers the appropriate consequence.
implementation: The community administrator is responsible for administering the jury process and carrying out the juries' conclusions.
oversight: The community administrator monitors the jury processes to ensure they appear fair.

# PROCESS
access: 
economics:
deliberation:
grievances:

# EVOLUTION
records: 
modification: A special jury may be called to consider a proposed modification to this Rule, and all jury members must agree to the modification for it to pass.
---

Use this template as-is or edit it to adapt it to your community.

Employs [sortition](https://medlabboulder.gitlab.io/democraticmediums/mediums/sortition/) and relies on the [Contributor Covenant](https://contributor-covenant.org/) for policies.
